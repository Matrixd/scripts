import re
import argparse
import os

def fix_reference_numbers(input_path, output_path):
    with open(input_path, 'r', encoding='utf-8') as f:
        lines = f.readlines()

    inline_refs = []
    ref_contents = {}
    new_lines = []

    for line in lines:
        # Check if the line has an inline reference
        inline_ref_matches = re.findall(r'\((\d+)\)', line)
        if inline_ref_matches:
            for match in inline_ref_matches:
                if match not in ref_contents:
                    inline_refs.append(match)

        # Check if the line is a reference definition
        ref_def_match = re.match(r'\((\d+)\) (.*)', line.strip())
        if ref_def_match and inline_refs:
            # Replace the number in the reference definition with the earliest inline reference number
            earliest_ref = inline_refs.pop(0)
            line = line.replace(ref_def_match.group(1), earliest_ref, 1)
            ref_contents[earliest_ref] = ref_def_match.group(2)

        new_lines.append(line)

    # Write the new lines to the output file
    with open(output_path, 'w', encoding='utf-8') as f:
        f.write(''.join(new_lines))
    return output_path




def replace_references(input_path, output_path):
    # Open the file and read all lines
    with open(input_path, 'r', encoding='utf-8') as f:
        lines = f.readlines()

    # Create a dictionary to store the references
    ref_dict = {}
    # Create a list to store the content lines
    content_lines = []

    for line in lines:
        # Check if the line has a reference
        match = re.search(r'\((\d+)\)', line)
        if match and match.group(1) == '1':
            # If a new set of references starts, replace references in the previous set and reset
            for i in range(len(content_lines)):
                for ref, text in ref_dict.items():
                    content_lines[i] = content_lines[i].replace(f'({ref})', f'<ref>{text}</ref>')
            ref_dict = {}

        # Check if the line is a reference definition
        match = re.match(r'\((\d+)\) (.*)', line.strip())
        if match:
            ref_dict[match.group(1)] = match.group(2)
        else:
            content_lines.append(line)

    # Replace references in the last set
    for i in range(len(content_lines)):
        for ref, text in ref_dict.items():
            content_lines[i] = content_lines[i].replace(f'({ref})', f'<ref>{text}</ref>')

    # Write the new content to file
    with open(output_path, 'w', encoding='utf-8') as f:
        f.write(''.join(content_lines))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Replace references in a text file')
    parser.add_argument('-s', '--source', type=str, help='Path to the source file')
    parser.add_argument('-o', '--output', type=str, help='Path to the output file')
    parser.add_argument('-f', '--fix-ref-count', type=str, help='Fix reference numbers in the source file: true or false, default is false')
    args = parser.parse_args()

    if args.source:
        if not args.output:
            args.output = 'output.txt'
        if args.fix_ref_count == 'true':
            temp_file = 'temp.txt'
            fix_reference_numbers(args.source, temp_file)
            replace_references(temp_file, args.output)
            os.remove(temp_file)
        else:
            replace_references(args.source, args.output)
    else:
        print("Error: No source file specified.")
        parser.print_help()
        exit(1)
    print("Replacement completed.")
    print(f"You can find the output file in: {args.output}")

